# Workshop Manifests

Contains the Compliance YAMLs and Ingress Installation YAMLs for use with the GitLab 
DevSecOps and Compliance Management Workshop.

See the [DevSecOps and Compliance Management Workshop]() for more details.

| Directory | Details |
| --------- | ------- |
| Compliance | Contains the Files used in the Compliance Pipelines lesson of the workshop |
| Deploy | Contains the Ingress-Nginx Controller used to expose the notes app |
